# Adaptive GtkSourceView style scheme

![](./screenshot.png)

Adaptive color scheme based on the GNOME palette.

## Installation

Copy the `adaptive.xml` file to a directory in the style search path (See ["How to install" on the GNOME wiki](https://wiki.gnome.org/Projects/GtkSourceView/StyleSchemes)).
